const hexToRgb = require('@zdaj/hex-to-rgb');

var imageFilters = (function() {

    var filters = [];

    var apply = function(name, pixels, args) {
        return filters[name] ? filters[name].call(null, pixels, args) : null;
    }

    filters['gradient'] = (pixels, colorObject) => {
        let beginColor = hexToRgb(colorObject['begin'] || '#0000ff');
        let endColor = hexToRgb(colorObject['end'] || '#ff3300');
        let len = parseFloat(colorObject['steps'] || 255);
        let scale = len / 255.0;

        let step = {
            r: (endColor.r - beginColor.r) / len,
            g: (endColor.g - beginColor.g) / len,
            b: (endColor.b - beginColor.b) / len
        }

        var d = pixels.data;
        for (var i = 0; i < d.length; i += 4) {
            var val = d[i] / scale;

            d[i] = beginColor.r + val * step.r;
            d[i + 1] = beginColor.g + val * step.g;
            d[i + 2] = beginColor.b + val * step.b;
            d[i + 3] = 255;
        }
    };

    tmpCanvas = document.createElement('canvas');
    tmpCtx = tmpCanvas.getContext('2d');

    createImageData = function(w, h) {
        return tmpCtx.createImageData(w, h);
    };

    filters['sharpen'] = (pixels) => {
        var result = convolute(pixels, [
            0, -1, 0, //
            -1, 5, -1, //
            0, -1, 0 //
        ]);
        for (var i = 0; i < result.data.length; i++)
            pixels.data[i] = result.data[i];
    };

    filters['sharpen3x3'] = filters['sharpen'];

    filters['sharpen5x5'] = (pixels) => {
        var result = convolute(pixels, [ //
            -1, -1, -1, -1, -1, //
            -1, -1, -1, -1, -1, //
            -1, -1, 25, -1, -1, //
            -1, -1, -1, -1, -1, //
            -1, -1, -1, -1, -1 //
        ]);
        for (var i = 0; i < result.data.length; i++)
            pixels.data[i] = result.data[i];
    }

    filters['blur'] = (pixels) => {
        var result = convolute(pixels, [
            1 / 9, 1 / 9, 1 / 9, //
            1 / 9, 1 / 9, 1 / 9, //
            1 / 9, 1 / 9, 1 / 9 //
        ]);
        for (var i = 0; i < result.data.length; i++)
            pixels.data[i] = result.data[i];
    };

    filters['enhance'] = (pixels) => {
        var result = convolute(pixels, [ //
            1 / 25, 1 / 25, 1 / 25, 1 / 25, 1 / 25, //
            1 / 25, 1 / 25, 1 / 25, 1 / 25, 1 / 25, //
            1 / 25, 1 / 25, 1 / 25, 1 / 25, 1 / 25, //
            1 / 25, 1 / 25, 1 / 25, 1 / 25, 1 / 25, //
            1 / 25, 1 / 25, 1 / 25, 1 / 25, 1 / 25, //
        ]);

        for (var i = 0; i < result.data.length; i++)
            pixels.data[i] = 2 * pixels.data[i] - result.data[i];
    }

    filters['custom'] = (pixels, args) => {
        var result = convolute(pixels, args);

        for (var i = 0; i < result.data.length; i++)
            pixels.data[i] = 2 * pixels.data[i] - result.data[i];
    }

    filters['sobel'] = (pixels) => {
        var horizontal = convolute(pixels, [ //
            -1, 0, 1, //
            -2, 0, 2, //
            -1, 0, 1, //
        ]);

        var vertical = convolute(pixels, [ //
            1, 2, 1, //
            0, 0, 0, //
            -1, -2, -1, //
        ]);

        for (var i = 0; i < pixels.data.length; i += 4) {
            pixels.data[i] = Math.abs(horizontal.data[i]);
            pixels.data[i + 1] = Math.abs(vertical.data[i]);
            pixels.data[i + 2] = (Math.abs(horizontal.data[i]) + Math.abs(vertical.data[i])) / 4;
        }
    }

    var convolute = (pixels, args) => {
        var side = Math.round(Math.sqrt(args.length));
        var halfSide = Math.floor(side / 2);
        var src = pixels.data;
        var w = pixels.width;
        var h = pixels.height;

        //var w = sw;
        //var h = sh;
        var output = createImageData(w, h);
        var dst = output.data;

        for (var y = 0; y < h; y++) {
            for (var x = 0; x < w; x++) {
                var sy = y;
                var sx = x;
                var dstOff = (y * w + x) * 4;

                var r = 0,
                    g = 0,
                    b = 0,
                    a = 0;
                for (var cy = 0; cy < side; cy++) {
                    for (var cx = 0; cx < side; cx++) {
                        var scy = sy + cy - halfSide;
                        var scx = sx + cx - halfSide;
                        if (scy >= 0 && scy < h && scx >= 0 && scx < w) {
                            var srcOff = (scy * w + scx) * 4;
                            var wt = args[cy * side + cx];
                            r += src[srcOff] * wt;
                            g += src[srcOff + 1] * wt;
                            b += src[srcOff + 2] * wt;
                            a += src[srcOff + 3] * wt;
                        }
                    }
                }
                dst[dstOff] = r;
                dst[dstOff + 1] = g;
                dst[dstOff + 2] = b;
                dst[dstOff + 3] = 255;
            }
        }
        return output;
    };

    return {
        apply: apply
    }
})();

module.exports = imageFilters;